FROM busybox:1

WORKDIR /scripts

COPY 00-create-user.sh .
COPY 01-clinical_data_schema.sql .

CMD cp /scripts/* /init-script-dir
