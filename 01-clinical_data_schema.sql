--
-- PostgreSQL database dump
--

-- Dumped from database version 10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.17 (Ubuntu 10.17-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: clinical_data_service; Type: DATABASE; Schema: -; Owner: clinical_data_service_user
--

CREATE DATABASE clinical_data_service;


ALTER DATABASE clinical_data_service OWNER TO clinical_data_service_user;

\connect clinical_data_service

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: doc_spec; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.doc_spec AS ENUM (
    'specialty1',
    'specialty2',
    '...',
    'Παθολόγος',
    'Γενικός Γιατρός',
    'Ωτορινολαρυγγολόγος'
);


ALTER TYPE public.doc_spec OWNER TO clinical_data_service_user;

--
-- Name: ext_bool; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.ext_bool AS ENUM (
    'Yes',
    'No',
    'Unknown'
);


ALTER TYPE public.ext_bool OWNER TO clinical_data_service_user;

--
-- Name: hosp; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.hosp AS ENUM (
    'ICU',
    'Life Support Outsice ICU',
    'Hospital Room'
);


ALTER TYPE public.hosp OWNER TO clinical_data_service_user;

--
-- Name: icd10; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.icd10 AS ENUM (
    'diagnosis1',
    'diagnosis2',
    '...',
    'UO7.2'
);


ALTER TYPE public.icd10 OWNER TO clinical_data_service_user;

--
-- Name: medhist; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.medhist AS ENUM (
    'NO',
    'CVD',
    'AH',
    'DM',
    'COPD',
    'CANCER',
    'AUTOIMMUNE'
);


ALTER TYPE public.medhist OWNER TO clinical_data_service_user;

--
-- Name: pcr_types; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.pcr_types AS ENUM (
    'ONE STEP RT-qPCR',
    'TWO STEP qPCR'
);


ALTER TYPE public.pcr_types OWNER TO clinical_data_service_user;

--
-- Name: prc_protocol_type; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.prc_protocol_type AS ENUM (
    'CE-IVD commercial kit',
    'Commercial Kit',
    'In house protocol'
);


ALTER TYPE public.prc_protocol_type OWNER TO clinical_data_service_user;

--
-- Name: sample_type; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.sample_type AS ENUM (
    'blood',
    'respiratory sample'
);


ALTER TYPE public.sample_type OWNER TO clinical_data_service_user;

--
-- Name: sequencer; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.sequencer AS ENUM (
    'Illumina MiSeq',
    'Illumina NextSeq',
    'Illumina NovaSeq',
    'Ion Torrent',
    'Nanopore MinION',
    'NovaSeq',
    'MiSeq',
    'NextSeq'
);


ALTER TYPE public.sequencer OWNER TO clinical_data_service_user;

--
-- Name: sex; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.sex AS ENUM (
    'M',
    'F'
);


ALTER TYPE public.sex OWNER TO clinical_data_service_user;

--
-- Name: storage_temp; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.storage_temp AS ENUM (
    '-80',
    '-20'
);


ALTER TYPE public.storage_temp OWNER TO clinical_data_service_user;

--
-- Name: stored_sample_type; Type: TYPE; Schema: public; Owner: clinical_data_service_user
--

CREATE TYPE public.stored_sample_type AS ENUM (
    'RNA',
    'DNA',
    'cells'
);


ALTER TYPE public.stored_sample_type OWNER TO clinical_data_service_user;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: b_lymphocyte_seq; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.b_lymphocyte_seq (
    id integer NOT NULL,
    sample_id integer NOT NULL,
    sequencing_date date,
    library_protocol character varying(500),
    sequencing_platform public.sequencer DEFAULT 'MiSeq'::public.sequencer,
    bio_pipeline character varying(500),
    result_file_link text
);


ALTER TABLE public.b_lymphocyte_seq OWNER TO clinical_data_service_user;

--
-- Name: b_lymphocyte_seq_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.b_lymphocyte_seq_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.b_lymphocyte_seq_id_seq OWNER TO clinical_data_service_user;

--
-- Name: b_lymphocyte_seq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.b_lymphocyte_seq_id_seq OWNED BY public.b_lymphocyte_seq.id;


--
-- Name: clinical_characteristics_timeline; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.clinical_characteristics_timeline (
    id integer NOT NULL,
    disease_timeline_id integer,
    clinical_characteristic_observed character varying(200) NOT NULL,
    clinical_characteristic_value real NOT NULL
);


ALTER TABLE public.clinical_characteristics_timeline OWNER TO clinical_data_service_user;

--
-- Name: clinical_characteristics_timeline_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.clinical_characteristics_timeline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clinical_characteristics_timeline_id_seq OWNER TO clinical_data_service_user;

--
-- Name: clinical_characteristics_timeline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.clinical_characteristics_timeline_id_seq OWNED BY public.clinical_characteristics_timeline.id;


--
-- Name: disease_instance; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.disease_instance (
    disease_instance_id integer NOT NULL,
    submitter_id integer NOT NULL,
    patient_id integer NOT NULL,
    symptom_onset_date date,
    patient_admission_date date,
    patient_discharge_date date,
    endemic_history public.ext_bool DEFAULT 'Unknown'::public.ext_bool,
    contact_w_confirmed_case public.ext_bool DEFAULT 'Unknown'::public.ext_bool,
    travel_history public.ext_bool DEFAULT 'Unknown'::public.ext_bool,
    travel_destination character varying(100),
    hospitalization public.ext_bool DEFAULT 'Unknown'::public.ext_bool,
    hospitalization_type public.hosp,
    medical_history public.medhist,
    symptom_duration integer,
    death boolean DEFAULT false
);


ALTER TABLE public.disease_instance OWNER TO clinical_data_service_user;

--
-- Name: disease_instance_disease_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.disease_instance_disease_instance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disease_instance_disease_instance_id_seq OWNER TO clinical_data_service_user;

--
-- Name: disease_instance_disease_instance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.disease_instance_disease_instance_id_seq OWNED BY public.disease_instance.disease_instance_id;


--
-- Name: disease_timeline; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.disease_timeline (
    disease_timeline_id integer NOT NULL,
    disease_instance_id integer,
    day integer NOT NULL
);


ALTER TABLE public.disease_timeline OWNER TO clinical_data_service_user;

--
-- Name: disease_timeline_disease_timeline_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.disease_timeline_disease_timeline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disease_timeline_disease_timeline_id_seq OWNER TO clinical_data_service_user;

--
-- Name: disease_timeline_disease_timeline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.disease_timeline_disease_timeline_id_seq OWNED BY public.disease_timeline.disease_timeline_id;



CREATE TABLE public.submitter (
    user_id integer NOT NULL,
    username character varying(100) NOT NULL,
    name character varying(100),
    email character varying(100),
    groups character varying(100)
);


ALTER TABLE public.submitter OWNER TO clinical_data_service_user;

--
-- Name: doctor; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.doctor (
    doctor_id integer NOT NULL,
    given_name character varying(100),
    family_name character varying(100),
    ssn character varying(100),
    specialty public.doc_spec,
    phone_number character varying(50),
    healthcare_unit character varying(300)
);


ALTER TABLE public.doctor OWNER TO clinical_data_service_user;

--
-- Name: doctor_doctor_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.doctor_doctor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doctor_doctor_id_seq OWNER TO clinical_data_service_user;

--
-- Name: doctor_doctor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.doctor_doctor_id_seq OWNED BY public.doctor.doctor_id;


--
-- Name: drug_received_timeline; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.drug_received_timeline (
    id integer NOT NULL,
    disease_timeline_id integer,
    drug_received character varying(200) NOT NULL,
    drug_dosage real NOT NULL
);


ALTER TABLE public.drug_received_timeline OWNER TO clinical_data_service_user;

--
-- Name: drug_received_timeline_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.drug_received_timeline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drug_received_timeline_id_seq OWNER TO clinical_data_service_user;

--
-- Name: drug_received_timeline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.drug_received_timeline_id_seq OWNED BY public.drug_received_timeline.id;


--
-- Name: drugs; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.drugs (
    drug_id integer NOT NULL,
    disease_instance_id integer NOT NULL,
    drug_name character varying(100) NOT NULL,
    drug_value character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE public.drugs OWNER TO clinical_data_service_user;

--
-- Name: drugs_drug_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.drugs_drug_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.drugs_drug_id_seq OWNER TO clinical_data_service_user;

--
-- Name: drugs_drug_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.drugs_drug_id_seq OWNED BY public.drugs.drug_id;


--
-- Name: immune_exam; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.immune_exam (
    id integer NOT NULL,
    sample_id integer NOT NULL,
    exam_date date,
    igm_title character varying(100),
    igg_title character varying(100)
);


ALTER TABLE public.immune_exam OWNER TO clinical_data_service_user;

--
-- Name: immune_exam_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.immune_exam_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.immune_exam_id_seq OWNER TO clinical_data_service_user;

--
-- Name: immune_exam_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.immune_exam_id_seq OWNED BY public.immune_exam.id;


--
-- Name: patient; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.patient (
    patient_id integer NOT NULL,
    parient_data_source character varying(100),
    patient_data_source_value character varying(100),
    given_name character varying(100),
    family_name character varying(100),
    patient_sex public.sex NOT NULL,
    birth_date date,
    death_date date,
    age integer,
    ssn character varying(100),
    social_security_status boolean,
    nationality character varying(100),
    phone_number character varying(50),
    region character varying(100),
    county character varying(100),
    zip_code character varying(100),
    city character varying(100),
    address character varying(200)
);


ALTER TABLE public.patient OWNER TO clinical_data_service_user;

--
-- Name: patient_genotyping; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.patient_genotyping (
    id integer NOT NULL,
    sample_id integer NOT NULL,
    genotyping_date date,
    method character varying(500),
    result_file_link text
);


ALTER TABLE public.patient_genotyping OWNER TO clinical_data_service_user;

--
-- Name: patient_genotyping_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.patient_genotyping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patient_genotyping_id_seq OWNER TO clinical_data_service_user;

--
-- Name: patient_genotyping_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.patient_genotyping_id_seq OWNED BY public.patient_genotyping.id;


--
-- Name: patient_patient_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.patient_patient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patient_patient_id_seq OWNER TO clinical_data_service_user;

--
-- Name: patient_patient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.patient_patient_id_seq OWNED BY public.patient.patient_id;


--
-- Name: patient_sequencing; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.patient_sequencing (
    id integer NOT NULL,
    sample_id integer NOT NULL,
    sequencing_date date,
    library_protocol character varying(500),
    sequencing_platform public.sequencer DEFAULT 'NovaSeq'::public.sequencer,
    bio_pipeline character varying(500),
    result_file_link text
);


ALTER TABLE public.patient_sequencing OWNER TO clinical_data_service_user;

--
-- Name: patient_sequencing_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.patient_sequencing_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patient_sequencing_id_seq OWNER TO clinical_data_service_user;

--
-- Name: patient_sequencing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.patient_sequencing_id_seq OWNED BY public.patient_sequencing.id;


--
-- Name: pcr; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.pcr (
    pcr_id integer NOT NULL,
    sample_id integer NOT NULL,
    sample_examination_date date,
    healthcare_unit character varying(300),
    pcr_method public.pcr_types,
    pcr_protocol public.prc_protocol_type,
    pcr_result_date date,
    pcr_result public.ext_bool DEFAULT 'No'::public.ext_bool
);


ALTER TABLE public.pcr OWNER TO clinical_data_service_user;

--
-- Name: pcr_gene_target; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.pcr_gene_target (
    gene_target_id integer NOT NULL,
    pcr_id integer,
    gene_target_name character varying(100) NOT NULL,
    gene_target_cycle_detection integer NOT NULL
);


ALTER TABLE public.pcr_gene_target OWNER TO clinical_data_service_user;

--
-- Name: pcr_gene_target_gene_target_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.pcr_gene_target_gene_target_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pcr_gene_target_gene_target_id_seq OWNER TO clinical_data_service_user;

--
-- Name: pcr_gene_target_gene_target_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.pcr_gene_target_gene_target_id_seq OWNED BY public.pcr_gene_target.gene_target_id;


--
-- Name: pcr_pcr_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.pcr_pcr_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pcr_pcr_id_seq OWNER TO clinical_data_service_user;

--
-- Name: pcr_pcr_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.pcr_pcr_id_seq OWNED BY public.pcr.pcr_id;


--
-- Name: referrals; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.referrals (
    referral_id integer NOT NULL,
    patient_id integer,
    doctor_id integer,
    referral_num character varying NOT NULL,
    disease_instance_id integer,
    issue_date date,
    icd10_diagnosis public.icd10,
    diagnosis text,
    examination_reason text
);


ALTER TABLE public.referrals OWNER TO clinical_data_service_user;

--
-- Name: referrals_referral_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.referrals_referral_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.referrals_referral_id_seq OWNER TO clinical_data_service_user;

--
-- Name: referrals_referral_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.referrals_referral_id_seq OWNED BY public.referrals.referral_id;


--
-- Name: risk_factors; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.risk_factors (
    risk_factor_id integer NOT NULL,
    disease_instance_id integer NOT NULL,
    risk_factor_name character varying(100) NOT NULL
);


ALTER TABLE public.risk_factors OWNER TO clinical_data_service_user;

--
-- Name: risk_factors_risk_factor_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.risk_factors_risk_factor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.risk_factors_risk_factor_id_seq OWNER TO clinical_data_service_user;

--
-- Name: risk_factors_risk_factor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.risk_factors_risk_factor_id_seq OWNED BY public.risk_factors.risk_factor_id;


--
-- Name: sample; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.sample (
    sample_id integer NOT NULL,
    referral_id integer NOT NULL,
    after_death_sample boolean DEFAULT false,
    initial_sample public.sample_type,
    stored_sample public.stored_sample_type,
    storage public.storage_temp,
    sample_collection_date date
);


ALTER TABLE public.sample OWNER TO clinical_data_service_user;

--
-- Name: sample_sample_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.sample_sample_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sample_sample_id_seq OWNER TO clinical_data_service_user;

--
-- Name: sample_sample_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.sample_sample_id_seq OWNED BY public.sample.sample_id;


--
-- Name: symptoms; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.symptoms (
    symptom_id integer NOT NULL,
    disease_instance_id integer NOT NULL,
    symptom_name character varying(100) NOT NULL
);


ALTER TABLE public.symptoms OWNER TO clinical_data_service_user;

--
-- Name: symptoms_symptom_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.symptoms_symptom_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.symptoms_symptom_id_seq OWNER TO clinical_data_service_user;

--
-- Name: symptoms_symptom_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.symptoms_symptom_id_seq OWNED BY public.symptoms.symptom_id;


--
-- Name: t_lymphocyte_seq; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.t_lymphocyte_seq (
    id integer NOT NULL,
    sample_id integer NOT NULL,
    sequencing_date date,
    library_protocol character varying(500),
    sequencing_platform public.sequencer DEFAULT 'NovaSeq'::public.sequencer,
    bio_pipeline character varying(500),
    result_file_link text
);


ALTER TABLE public.t_lymphocyte_seq OWNER TO clinical_data_service_user;

--
-- Name: t_lymphocyte_seq_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.t_lymphocyte_seq_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_lymphocyte_seq_id_seq OWNER TO clinical_data_service_user;

--
-- Name: t_lymphocyte_seq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.t_lymphocyte_seq_id_seq OWNED BY public.t_lymphocyte_seq.id;


--
-- Name: virus_sequencing; Type: TABLE; Schema: public; Owner: clinical_data_service_user
--

CREATE TABLE public.virus_sequencing (
    id integer NOT NULL,
    sample_id integer NOT NULL,
    sequencing_date date,
    analysis_lab character varying(500),
    library_protocol character varying(500),
    sequencing_platform public.sequencer,
    bio_pipeline character varying(500),
    num_seq_for_consensus integer,
    result_file_link text,
    avg_coverage real,
    avg_sequence_length real,
    covid_variant character varying(200)
);


ALTER TABLE public.virus_sequencing OWNER TO clinical_data_service_user;

--
-- Name: virus_sequencing_id_seq; Type: SEQUENCE; Schema: public; Owner: clinical_data_service_user
--

CREATE SEQUENCE public.virus_sequencing_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.virus_sequencing_id_seq OWNER TO clinical_data_service_user;

--
-- Name: virus_sequencing_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: clinical_data_service_user
--

ALTER SEQUENCE public.virus_sequencing_id_seq OWNED BY public.virus_sequencing.id;


--
-- Name: b_lymphocyte_seq id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.b_lymphocyte_seq ALTER COLUMN id SET DEFAULT nextval('public.b_lymphocyte_seq_id_seq'::regclass);


--
-- Name: clinical_characteristics_timeline id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.clinical_characteristics_timeline ALTER COLUMN id SET DEFAULT nextval('public.clinical_characteristics_timeline_id_seq'::regclass);


--
-- Name: disease_instance disease_instance_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_instance ALTER COLUMN disease_instance_id SET DEFAULT nextval('public.disease_instance_disease_instance_id_seq'::regclass);


--
-- Name: disease_timeline disease_timeline_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_timeline ALTER COLUMN disease_timeline_id SET DEFAULT nextval('public.disease_timeline_disease_timeline_id_seq'::regclass);


--
-- Name: doctor doctor_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.doctor ALTER COLUMN doctor_id SET DEFAULT nextval('public.doctor_doctor_id_seq'::regclass);


--
-- Name: drug_received_timeline id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drug_received_timeline ALTER COLUMN id SET DEFAULT nextval('public.drug_received_timeline_id_seq'::regclass);


--
-- Name: drugs drug_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drugs ALTER COLUMN drug_id SET DEFAULT nextval('public.drugs_drug_id_seq'::regclass);


--
-- Name: immune_exam id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.immune_exam ALTER COLUMN id SET DEFAULT nextval('public.immune_exam_id_seq'::regclass);


--
-- Name: patient patient_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient ALTER COLUMN patient_id SET DEFAULT nextval('public.patient_patient_id_seq'::regclass);


--
-- Name: patient_genotyping id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_genotyping ALTER COLUMN id SET DEFAULT nextval('public.patient_genotyping_id_seq'::regclass);


--
-- Name: patient_sequencing id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_sequencing ALTER COLUMN id SET DEFAULT nextval('public.patient_sequencing_id_seq'::regclass);


--
-- Name: pcr pcr_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr ALTER COLUMN pcr_id SET DEFAULT nextval('public.pcr_pcr_id_seq'::regclass);


--
-- Name: pcr_gene_target gene_target_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr_gene_target ALTER COLUMN gene_target_id SET DEFAULT nextval('public.pcr_gene_target_gene_target_id_seq'::regclass);


--
-- Name: referrals referral_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals ALTER COLUMN referral_id SET DEFAULT nextval('public.referrals_referral_id_seq'::regclass);


--
-- Name: risk_factors risk_factor_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.risk_factors ALTER COLUMN risk_factor_id SET DEFAULT nextval('public.risk_factors_risk_factor_id_seq'::regclass);


--
-- Name: sample sample_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.sample ALTER COLUMN sample_id SET DEFAULT nextval('public.sample_sample_id_seq'::regclass);


--
-- Name: symptoms symptom_id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.symptoms ALTER COLUMN symptom_id SET DEFAULT nextval('public.symptoms_symptom_id_seq'::regclass);


--
-- Name: t_lymphocyte_seq id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.t_lymphocyte_seq ALTER COLUMN id SET DEFAULT nextval('public.t_lymphocyte_seq_id_seq'::regclass);


--
-- Name: virus_sequencing id; Type: DEFAULT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.virus_sequencing ALTER COLUMN id SET DEFAULT nextval('public.virus_sequencing_id_seq'::regclass);


--
-- Name: b_lymphocyte_seq b_lymphocyte_seq_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.b_lymphocyte_seq
    ADD CONSTRAINT b_lymphocyte_seq_pkey PRIMARY KEY (id);


--
-- Name: clinical_characteristics_timeline clinical_characteristics_timeline_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.clinical_characteristics_timeline
    ADD CONSTRAINT clinical_characteristics_timeline_pkey PRIMARY KEY (id);


--
-- Name: disease_instance disease_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_instance
    ADD CONSTRAINT disease_instance_pkey PRIMARY KEY (disease_instance_id);


--
-- Name: disease_timeline disease_timeline_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_timeline
    ADD CONSTRAINT disease_timeline_pkey PRIMARY KEY (disease_timeline_id);



ALTER TABLE ONLY public.submitter
    ADD CONSTRAINT submitter_pkey PRIMARY KEY (user_id);


--
-- Name: doctor doctor_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.doctor
    ADD CONSTRAINT doctor_pkey PRIMARY KEY (doctor_id);


--
-- Name: drug_received_timeline drug_received_timeline_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drug_received_timeline
    ADD CONSTRAINT drug_received_timeline_pkey PRIMARY KEY (id);


--
-- Name: drugs drugs_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drugs
    ADD CONSTRAINT drugs_pkey PRIMARY KEY (drug_id);


--
-- Name: immune_exam immune_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.immune_exam
    ADD CONSTRAINT immune_exam_pkey PRIMARY KEY (id);


--
-- Name: patient_genotyping patient_genotyping_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_genotyping
    ADD CONSTRAINT patient_genotyping_pkey PRIMARY KEY (id);


--
-- Name: patient patient_patient_data_source_value_key; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT patient_patient_data_source_value_key UNIQUE (patient_data_source_value);


--
-- Name: patient patient_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT patient_pkey PRIMARY KEY (patient_id);


--
-- Name: patient_sequencing patient_sequencing_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_sequencing
    ADD CONSTRAINT patient_sequencing_pkey PRIMARY KEY (id);


--
-- Name: patient patient_ssn_key; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient
    ADD CONSTRAINT patient_ssn_key UNIQUE (ssn);


--
-- Name: pcr_gene_target pcr_gene_target_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr_gene_target
    ADD CONSTRAINT pcr_gene_target_pkey PRIMARY KEY (gene_target_id);


--
-- Name: pcr pcr_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr
    ADD CONSTRAINT pcr_pkey PRIMARY KEY (pcr_id);


--
-- Name: referrals referrals_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_pkey PRIMARY KEY (referral_id);


--
-- Name: referrals referrals_referral_num_key; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_referral_num_key UNIQUE (referral_num);


--
-- Name: risk_factors risk_factors_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.risk_factors
    ADD CONSTRAINT risk_factors_pkey PRIMARY KEY (risk_factor_id);


--
-- Name: sample sample_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.sample
    ADD CONSTRAINT sample_pkey PRIMARY KEY (sample_id);


--
-- Name: symptoms symptoms_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.symptoms
    ADD CONSTRAINT symptoms_pkey PRIMARY KEY (symptom_id);


--
-- Name: t_lymphocyte_seq t_lymphocyte_seq_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.t_lymphocyte_seq
    ADD CONSTRAINT t_lymphocyte_seq_pkey PRIMARY KEY (id);


--
-- Name: virus_sequencing virus_sequencing_pkey; Type: CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.virus_sequencing
    ADD CONSTRAINT virus_sequencing_pkey PRIMARY KEY (id);


--
-- Name: b_lymph_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX b_lymph_to_sample_index ON public.b_lymphocyte_seq USING btree (sample_id);


--
-- Name: clinical_characteristic_observed_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX clinical_characteristic_observed_index ON public.clinical_characteristics_timeline USING btree (clinical_characteristic_observed);


--
-- Name: disease_timeline_day_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX disease_timeline_day_index ON public.disease_timeline USING btree (day);

CREATE INDEX submitter_username_index ON public.submitter USING btree (username);

--
-- Name: disease_timeline_to_instance_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX disease_timeline_to_instance_index ON public.disease_timeline USING btree (disease_instance_id);


--
-- Name: disease_to_patient_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX disease_to_patient_index ON public.disease_instance USING btree (patient_id);



--
-- Name: drug_received_timeline_drug_received_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX drug_received_timeline_drug_received_index ON public.drug_received_timeline USING btree (drug_received);


--
-- Name: drug_to_disease_instance; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX drug_to_disease_instance ON public.drugs USING btree (disease_instance_id);


--
-- Name: gene_target_to_pcr_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX gene_target_to_pcr_index ON public.pcr_gene_target USING btree (pcr_id);


--
-- Name: immune_examp_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX immune_examp_to_sample_index ON public.immune_exam USING btree (sample_id);


--
-- Name: patient_gen_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX patient_gen_to_sample_index ON public.patient_genotyping USING btree (sample_id);


--
-- Name: patient_seq_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX patient_seq_to_sample_index ON public.patient_sequencing USING btree (sample_id);


--
-- Name: pcr_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX pcr_to_sample_index ON public.pcr USING btree (sample_id);


--
-- Name: referral_num_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX referral_num_index ON public.referrals USING btree (referral_num);


--
-- Name: referral_to_disease_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX referral_to_disease_index ON public.referrals USING btree (disease_instance_id);


--
-- Name: referral_to_doctor_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX referral_to_doctor_index ON public.referrals USING btree (doctor_id);


--
-- Name: risk_factor_to_disease_instance; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX risk_factor_to_disease_instance ON public.risk_factors USING btree (disease_instance_id);


--
-- Name: sample_to_referral_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX sample_to_referral_index ON public.sample USING btree (referral_id);


--
-- Name: symptom_symptom_name_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX symptom_symptom_name_index ON public.symptoms USING btree (symptom_name);


--
-- Name: symptom_to_disease_instance; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX symptom_to_disease_instance ON public.symptoms USING btree (disease_instance_id);


--
-- Name: t_lymph_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX t_lymph_to_sample_index ON public.t_lymphocyte_seq USING btree (sample_id);


--
-- Name: virus_seq_to_sample_index; Type: INDEX; Schema: public; Owner: clinical_data_service_user
--

CREATE INDEX virus_seq_to_sample_index ON public.virus_sequencing USING btree (sample_id);


--
-- Name: b_lymphocyte_seq b_lymphocyte_seq_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--





ALTER TABLE ONLY public.b_lymphocyte_seq
    ADD CONSTRAINT b_lymphocyte_seq_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: b_lymphocyte_seq b_lymphocyte_seq_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.b_lymphocyte_seq
    ADD CONSTRAINT b_lymphocyte_seq_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: clinical_characteristics_timeline clinical_characteristics_timeline_disease_timeline_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.clinical_characteristics_timeline
    ADD CONSTRAINT clinical_characteristics_timeline_disease_timeline_id_fkey FOREIGN KEY (disease_timeline_id) REFERENCES public.disease_timeline(disease_timeline_id);


--
-- Name: clinical_characteristics_timeline clinical_characteristics_timeline_disease_timeline_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.clinical_characteristics_timeline
    ADD CONSTRAINT clinical_characteristics_timeline_disease_timeline_id_fkey1 FOREIGN KEY (disease_timeline_id) REFERENCES public.disease_timeline(disease_timeline_id);


--
-- Name: disease_instance disease_instance_patient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_instance
    ADD CONSTRAINT disease_instance_patient_id_fkey FOREIGN KEY (patient_id) REFERENCES public.patient(patient_id);


--
-- Name: disease_instance disease_instance_patient_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_instance
    ADD CONSTRAINT disease_instance_patient_id_fkey1 FOREIGN KEY (patient_id) REFERENCES public.patient(patient_id);




ALTER TABLE ONLY public.disease_instance
    ADD CONSTRAINT disease_instance_submitter_id_fkey FOREIGN KEY (submitter_id) REFERENCES public.submitter(user_id);



ALTER TABLE ONLY public.disease_instance
    ADD CONSTRAINT disease_instance_submitter_id_fkey1 FOREIGN KEY (submitter_id) REFERENCES public.submitter(user_id);




--
-- Name: disease_timeline disease_timeline_disease_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_timeline
    ADD CONSTRAINT disease_timeline_disease_instance_id_fkey FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: disease_timeline disease_timeline_disease_instance_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.disease_timeline
    ADD CONSTRAINT disease_timeline_disease_instance_id_fkey1 FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: drug_received_timeline drug_received_timeline_disease_timeline_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drug_received_timeline
    ADD CONSTRAINT drug_received_timeline_disease_timeline_id_fkey FOREIGN KEY (disease_timeline_id) REFERENCES public.disease_timeline(disease_timeline_id);


--
-- Name: drug_received_timeline drug_received_timeline_disease_timeline_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drug_received_timeline
    ADD CONSTRAINT drug_received_timeline_disease_timeline_id_fkey1 FOREIGN KEY (disease_timeline_id) REFERENCES public.disease_timeline(disease_timeline_id);


--
-- Name: drugs drugs_disease_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drugs
    ADD CONSTRAINT drugs_disease_instance_id_fkey FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: drugs drugs_disease_instance_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.drugs
    ADD CONSTRAINT drugs_disease_instance_id_fkey1 FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: immune_exam immune_exam_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.immune_exam
    ADD CONSTRAINT immune_exam_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: immune_exam immune_exam_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.immune_exam
    ADD CONSTRAINT immune_exam_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: patient_genotyping patient_genotyping_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_genotyping
    ADD CONSTRAINT patient_genotyping_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: patient_genotyping patient_genotyping_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_genotyping
    ADD CONSTRAINT patient_genotyping_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: patient_sequencing patient_sequencing_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_sequencing
    ADD CONSTRAINT patient_sequencing_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: patient_sequencing patient_sequencing_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.patient_sequencing
    ADD CONSTRAINT patient_sequencing_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: pcr_gene_target pcr_gene_target_pcr_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr_gene_target
    ADD CONSTRAINT pcr_gene_target_pcr_id_fkey FOREIGN KEY (pcr_id) REFERENCES public.pcr(pcr_id);


--
-- Name: pcr_gene_target pcr_gene_target_pcr_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr_gene_target
    ADD CONSTRAINT pcr_gene_target_pcr_id_fkey1 FOREIGN KEY (pcr_id) REFERENCES public.pcr(pcr_id);


--
-- Name: pcr pcr_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr
    ADD CONSTRAINT pcr_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: pcr pcr_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.pcr
    ADD CONSTRAINT pcr_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: referrals referrals_disease_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_disease_instance_id_fkey FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: referrals referrals_disease_instance_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_disease_instance_id_fkey1 FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: referrals referrals_doctor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_doctor_id_fkey FOREIGN KEY (doctor_id) REFERENCES public.doctor(doctor_id);


--
-- Name: referrals referrals_doctor_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_doctor_id_fkey1 FOREIGN KEY (doctor_id) REFERENCES public.doctor(doctor_id);


--
-- Name: referrals referrals_patient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_patient_id_fkey FOREIGN KEY (patient_id) REFERENCES public.patient(patient_id);


--
-- Name: referrals referrals_patient_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.referrals
    ADD CONSTRAINT referrals_patient_id_fkey1 FOREIGN KEY (patient_id) REFERENCES public.patient(patient_id);


--
-- Name: risk_factors risk_factors_disease_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.risk_factors
    ADD CONSTRAINT risk_factors_disease_instance_id_fkey FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: risk_factors risk_factors_disease_instance_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.risk_factors
    ADD CONSTRAINT risk_factors_disease_instance_id_fkey1 FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: sample sample_referral_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.sample
    ADD CONSTRAINT sample_referral_id_fkey FOREIGN KEY (referral_id) REFERENCES public.referrals(referral_id);


--
-- Name: sample sample_referral_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.sample
    ADD CONSTRAINT sample_referral_id_fkey1 FOREIGN KEY (referral_id) REFERENCES public.referrals(referral_id);


--
-- Name: symptoms symptoms_disease_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.symptoms
    ADD CONSTRAINT symptoms_disease_instance_id_fkey FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: symptoms symptoms_disease_instance_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.symptoms
    ADD CONSTRAINT symptoms_disease_instance_id_fkey1 FOREIGN KEY (disease_instance_id) REFERENCES public.disease_instance(disease_instance_id);


--
-- Name: t_lymphocyte_seq t_lymphocyte_seq_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.t_lymphocyte_seq
    ADD CONSTRAINT t_lymphocyte_seq_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: t_lymphocyte_seq t_lymphocyte_seq_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.t_lymphocyte_seq
    ADD CONSTRAINT t_lymphocyte_seq_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: virus_sequencing virus_sequencing_sample_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.virus_sequencing
    ADD CONSTRAINT virus_sequencing_sample_id_fkey FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: virus_sequencing virus_sequencing_sample_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: clinical_data_service_user
--

ALTER TABLE ONLY public.virus_sequencing
    ADD CONSTRAINT virus_sequencing_sample_id_fkey1 FOREIGN KEY (sample_id) REFERENCES public.sample(sample_id);


--
-- Name: TABLE b_lymphocyte_seq; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.b_lymphocyte_seq TO metabase_client_user;


--
-- Name: TABLE clinical_characteristics_timeline; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.clinical_characteristics_timeline TO metabase_client_user;


--
-- Name: TABLE disease_instance; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.disease_instance TO metabase_client_user;


--
-- Name: TABLE disease_timeline; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.disease_timeline TO metabase_client_user;


--
-- Name: TABLE doctor; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.doctor TO metabase_client_user;


--
-- Name: TABLE drug_received_timeline; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.drug_received_timeline TO metabase_client_user;


--
-- Name: TABLE drugs; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.drugs TO metabase_client_user;


--
-- Name: TABLE immune_exam; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.immune_exam TO metabase_client_user;


--
-- Name: TABLE patient; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

--GRANT SELECT(patient_id, parient_data_source, patient_data_source_value, patient_sex, birth_date, death_date, age, social_security_status, nationality, region, county, zip_code, city) ON TABLE public.patient TO metabase_client_user;
GRANT SELECT ON TABLE public.patient TO metabase_client_user;


--
-- Name: TABLE patient_genotyping; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.patient_genotyping TO metabase_client_user;


--
-- Name: TABLE patient_sequencing; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.patient_sequencing TO metabase_client_user;


--
-- Name: TABLE pcr; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.pcr TO metabase_client_user;


--
-- Name: TABLE pcr_gene_target; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.pcr_gene_target TO metabase_client_user;


--
-- Name: TABLE referrals; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.referrals TO metabase_client_user;


--
-- Name: TABLE risk_factors; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.risk_factors TO metabase_client_user;


--
-- Name: TABLE sample; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.sample TO metabase_client_user;


--
-- Name: TABLE symptoms; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.symptoms TO metabase_client_user;


--
-- Name: TABLE t_lymphocyte_seq; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.t_lymphocyte_seq TO metabase_client_user;


--
-- Name: TABLE virus_sequencing; Type: ACL; Schema: public; Owner: clinical_data_service_user
--

GRANT SELECT ON TABLE public.virus_sequencing TO metabase_client_user;


--
-- PostgreSQL database dump complete
--

