#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER clinical_data_service_user WITH PASSWORD '$CLINICAL_DATA_SERVICE_PASS';
    CREATE USER metabase_client_user WITH PASSWORD '$METABASE_CLIENT_PASS';
    CREATE USER metabase WITH PASSWORD '$MB_DB_PASS';
EOSQL
